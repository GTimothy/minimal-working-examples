#!/bin/bash

# paramètres de la fonction SBATCH utilisée pour lancer ce script. Un paramètre du script est ignoré s il est spécifié en ligne de commande pendant l
#SBATCH --qos qos_gpu-gc
#SBATCH --gres=gpu:4 		# nombre de GPU par noeud (contrainte matérielle à respecter)
#SBATCH --ntasks=5 		# nombre de tâches
#SBATCH --cpus-per-gpu=2 	# nombre de coeurs à réserver par GPU
#SBATCH --gpus-per-task=4	# nombre de GPU par tâche
#SBATCH --threads-per-core=1	# on réserve des coeurs physiques et non logiques
#SBATCH --time=2:00:00		# temps exécution maximum demande (HH:MM:SS)


# chargement des modules
module load pytorch-gpu/py3/1.4.0


# execution de code
echo
echo "Ceci n'est exécuté que sur le noeud master"
python -c "import torch, socket; print(f'{torch.cuda.device_count()} GPUs visibles sur le noeud {socket.gethostname()}')"
echo
echo "Ceci est exécuté sur tous les noeuds simultanément"
srun --gres=gpu:4 python -c "import torch, socket; print(f'{torch.cuda.device_count()} GPUs visibles sur le noeud {socket.gethostname()}')"
