#!/bin/bash

master_addr=$1
master_port=$2
python -m torch.distributed.launch \
  --nnodes 2 \
  --node_rank=$SLURM_PROCID \
  --nproc_per_node=4 \
  --master_addr=$master_addr \
  --master_port=$master_port \
  pytorch_mwe_distributedtraining.py --n_epochs 5