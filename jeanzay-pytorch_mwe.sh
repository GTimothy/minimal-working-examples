#!/bin/bash

# paramètres de la fonction SBATCH utilisée pour lancer ce script. Un paramètre du script est ignoré s il est spécifié en ligne de commande pendant l
#SBATCH --qos qos_gpu-gc
#SBATCH --gres=gpu:4 		# nombre de GPU par noeud (contrainte matérielle à respecter)
#SBATCH --ntasks=2 		# nombre de tâches
#SBATCH --cpus-per-gpu=2 	# nombre de coeurs à réserver par GPU
#SBATCH --gpus-per-task=4	# nombre de GPU par tâche
#SBATCH --threads-per-core=1	# on réserve des coeurs physiques et non logiques
#SBATCH --time=2:00:00		# temps exécution maximum demande (HH:MM:SS)


# chargement des modules
module load pytorch-gpu/py3/1.4.0


# execution de code
echo
echo "Ceci n'est exécuté que sur le noeud master"
master_addr=$(hostname)
master_port=$(python -c "import socket; s = socket.socket(); s.bind(('', 0)); print(s.getsockname()[1])")
echo master:$master_addr
echo master_port:$master_port

echo "Ceci est exécuté sur tous les noeuds simultanément"
srun --gres=gpu:4 -N 2 ./jeanzay-pytorch_mwe_srun.sh $master_addr $master_port


