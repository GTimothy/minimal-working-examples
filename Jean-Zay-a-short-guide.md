# Account


Account creation on the [EDARI](https://www.edari.fr/) website following the procedure in :
http://www.genci.fr/sites/default/files/2019-Modalitesdacces.pdf



# Connection

Summarised and translated from [access documentation](http://www.idris.fr/jean-zay/cpu/jean-zay-cpu-connexion-et-shells.html)

Once you have an Idris account, you can use your `idris_login` and
`idris_password` to connect to the cluster using the SSH protocol:
```shell
 ssh idris_login@jean-zay.idris.fr
```
 then type your `idris_password`.


It is possible (and I recommend it) to use `ssh keys` for a secure password-
less connection.  
See: http://www.idris.fr/faqs/ssh_keys.html  
When this is set up properly, no need to type your password.

```shell
 ssh idris_login@jean-zay.idris.fr
```


In my case, I have aliased `jean-zay.idris.fr` with `jz` in my ssh config and
I've set up my login by default, which means I can connect to Jean Zay using
```shell
ssh jz
```

If you have issues with your initial password : [Details on idris passwords](http://www.idris.fr/eng/faqs/password-pb-eng.html#example_of_using_and_changing_an_initial_password_to_an_actual_password)

# Usage

Official SLURM cheat sheet:
https://slurm.schedmd.com/pdfs/summary.pdf

## squeue
 to list every job queued on the cluster for every user, with the ressource used, the current state, the time since started running (if running).  
 [Link to SLURM documentation](https://slurm.schedmd.com/squeue.html)


Restrict it to
 - a user with `--users=<user_id>`
 - an 'account' (can contain multiple users) with `--acount=<acount>`




## sbatch
to queue a job : `sbatch <job_slurm_script>` .  
[Link to SLURM documentation](https://slurm.schedmd.com/sbatch.html)

Job parameters can be passed as `sbatch` arguments before the job script, or can be specified inside the job script with an `#SBATCH` prefix.

>When the job allocation is finally granted for the batch script, Slurm runs a single copy of the batch script on the first node in the set of allocated nodes.

In the script, work can be distributed to other reserved nodes using the `srun` command. That is, by default the code in the script is run on the first node allocated, but code prefixed with the `srun` command can be run on multiple nodes in parallel.


## srun
to run a parallel job

specify which/how many nodes to use as argument before passing the code to be distributed.

See the minimal working example.
