# Minimal Working Examples

## Files:
#### PyTorch model training, with or without distributed training
 |File|Description|  
 |---|---|
 |[pytorch_mwe_distributedtraining.py](pytorch_mwe_distributedtraining.py)|Python script to train a toy model to calculate `a * b`. <br> Launched using the [torch.distributed.launch](https://github.com/pytorch/pytorch/blob/master/torch/distributed/launch.py) utility.|
 |[pytorch_mwe_requirements.txt](pytorch_mwe_requirements.txt)| Python package requirements for the this MWE.|

#### SLURM simple multi-node multi-GPU job:  
 |File|Description|  
 |---|---|
 |[jeanzay_mwe.sh](jeanzay_mwe.sh)|SLURM script to run on Jean Zay with SLURM's [sbatch](https://slurm.schedmd.com/sbatch.html) command.|
 |[jeanzay_mwe.p18.pdf](jeanzay_mwe.p18.pdf)| Script pdf with expected result.|

 #### asciinema-cast/*
 asciicast recordings of me running the scripts. Can be replayed in your terminal with [asciinema](https://asciinema.org/docs/usage).


## Running the examples:
### PyTorch
- **train on a single node (localhost) starting 4 processes (each using 1 GPU here) during 10 epochs:**  
```shell
python -m torch.distributed.launch --nproc_per_node=4 pytorch_mwe_distributedtraining.py --n_epochs=10
```

- **make *GPU:0* invisible (if used by someone else) + train on a single node (localhost) during 10 epochs by starting 3 processes:**  
```shell
CUDA_VISIBLE_DEVICES=1,2,3 python -m torch.distributed.launch --nproc_per_node=3 pytorch_mwe_distributedtraining.py --n_epochs=10
```

**IMPORTANT:** *torch.distributed.launch* simply sets environment variables and launches the script *nproc_per_node* times, each time appending a `--local-rank=<n>` argument ranging from `0` to `nproc_per_node` (excluded). It is then up to the programmer to
 - call `torch.distributed.init_process_group(backend='nccl', init_method='env://')` in his code to link all the processes using the aforementioned environment variables.
 - decide which GPU(s) to use in his script.  
 In this MWE, we use one GPU to train the model, and we tell PyTorch that the device to use will be *cuda:local-rank*   

### SLURM
 - **submit the job defined in `jeanzay_mwe.p18.sh`:**  
 ```bash
 sbatch jeanzay_mwe.p18.sh
 ```

### SLURM + PyTorch
 - *train on 8 GPUs (2 nodes with 4 GPUs each)*
   ```bash
   sbatch jeanzay-pytorch_mwe.sh
   ```
   The logic here is to:
   - prepare the material requirements and software environment
   - get an open port on the master node to setup node communication later
   - run `jeanzay-pytorch_mwe_srun.sh` on each node, in which
    - `$SLURM_PROCID` can be used as `node_rank`
    - run `python -m torch.distributed.launch` with the all the proper parameters (`nnodes`, `node_rank`, `nproc_per_node`, `master_addr`, `master_port`) to start  4 instances of `jeanzay-pytorch_mwe.py` per 4GPU node
