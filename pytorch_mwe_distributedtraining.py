import sys
import torch
from torch import distributed, nn, optim
import torch.utils.data

from tqdm import tqdm  # for progress visualisation, nothing to do with deep learning


class ToyModel(nn.Module):
    def __init__(self):
        super(ToyModel, self).__init__()
        self.net1 = nn.Linear(2, 10)
        self.relu = nn.ReLU()
        self.net2 = nn.Linear(10, 1)

    def forward(self, x):
        return self.net2(self.relu(self.net1(x)))


def fct_to_learn(tensor):
    return tensor.prod(-1)


def generate_dataset(a_start, a_end, b_start, b_end, n, seed=42):
    torch.manual_seed(seed)
    X = torch.stack([
        (a_end-a_start)*torch.rand((n))+a_start,
        (b_end-b_start)*torch.rand((n))+b_start]).transpose(0, 1)
    Y = fct_to_learn(X).unsqueeze(-1)
    return torch.utils.data.dataset.TensorDataset(X, Y)


def is_first_gpu():
    return not torch.distributed.is_initialized() or torch.distributed.get_rank() == 0


def demo_basic(epochs, show_progress=False):
    device = torch.device(torch.cuda.current_device() if torch.cuda.is_available() else 'cpu')

    # generating train and valid datasets using the function to approximate
    train_dataset = generate_dataset(a_start=0, a_end=5, b_start=0, b_end=10, n=100000, seed=42) +\
                    generate_dataset(a_start=6, a_end=10, b_start=0, b_end=10, n=100000, seed=43)
    valid_dataset = generate_dataset(a_start=5, a_end=6, b_start=0, b_end=10, n=1000, seed=666)

    # if distributed training, do sampling to restrict data to different subset for each node
    sampler = None if not torch.distributed.is_initialized() else torch.utils.data.distributed.DistributedSampler(
        dataset=train_dataset)
    train_dataloader = torch.utils.data.dataloader.DataLoader(train_dataset, batch_size=1000, sampler=sampler)
    valid_dataloader = torch.utils.data.dataloader.DataLoader(valid_dataset, batch_size=1000)

    # create model and move it to the device in use (if cpu, its a no-op)
    model = ToyModel().to(device)
    if torch.distributed.is_initialized():
        # wrapping model for distributed training (adds synchronizations)
        ddp_model = torch.nn.parallel.distributed.DistributedDataParallel(model, device_ids=[device.index],
                                                                          output_device=device.index)
        model = ddp_model

    loss_fn = nn.MSELoss()
    optimizer = optim.SGD(model.parameters(), lr=1e-3)

    def run_epoch(dataloader, train=True):
        model.train(train)

        c_loss = 0
        for X_batch, Y_batch in tqdm(dataloader, disable=not show_progress):
            X_batch, Y_batch = X_batch.to(device), Y_batch.to(device)
            Y_batch_pred = model(X_batch)
            loss = loss_fn(Y_batch_pred, Y_batch)
            c_loss += loss.item()
            if train:
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
        return c_loss

    for epoch in range(epochs):
        if is_first_gpu(): print(f"###\nEpoch {epoch}")
        c_loss = run_epoch(train_dataloader, train=True)
        print(f"Training : Avg TRAIN loss: {c_loss / len(train_dataloader)}")

        if torch.distributed.is_initialized():
            torch.distributed.barrier()
        if is_first_gpu():
            print("Starting evaluation loop")
            c_loss = run_epoch(train_dataloader, train=False)
            print(f"Evaluating : Avg TRAIN loss: {c_loss / len(train_dataloader)}")
            c_loss = run_epoch(valid_dataloader, train=False)
            print(f"Evaluating : Avg VALID loss: {c_loss / len(valid_dataloader)}")


    # Here we save our model (pytorch.org/tutorials/beginner/saving_loading_models.html)
    if is_first_gpu():
        model = model.module if isinstance(model, torch.nn.parallel.distributed.DistributedDataParallel) else model
        torch.save(model.state_dict(), 'trained_toy_model.state_dict')

def load_and_play_with_model(model_state_dict, device='cpu'):
    device = torch.device(device)
    model = ToyModel().to(device)
    model.load_state_dict(torch.load(model_state_dict, map_location=device))
    # Here we test our model manually
    while True:
        a = float(input('enter \'a\' value: '))
        b = float(input('enter \'b\' value: '))
        model_input = torch.tensor([[a,b]], device=device)
        print('...calculating expected result...')
        print(fct_to_learn(model_input))
        print('...calculating result using model...')
        print(model(model_input))

if __name__ == '__main__':
    import argparse, os
    parser = argparse.ArgumentParser()
    parser.add_argument("--local_rank", required=False, type=int)
    parser.add_argument("--n_epochs", required=True, type=int)
    parser.add_argument("--show_progress", default=False, type=bool)
    args = parser.parse_args()
    if args.local_rank is not None:
        torch.distributed.init_process_group(backend='nccl',
                                             init_method='env://')
        torch.cuda.set_device(args.local_rank)

    if torch.distributed.is_initialized():
        print(f"Running as distributed node {os.environ['SLURM_PROCID']+', ' if 'SLURM_PROCID' in os.environ else ''}"
              f"with local rank {torch.distributed.get_rank()}, global rank {os.environ['RANK']}")
        torch.distributed.barrier() # synchronizes all processes

    demo_basic(args.n_epochs, show_progress=args.show_progress)



